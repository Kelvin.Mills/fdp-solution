//Concrete Product
public class BeefBurger implements Burger {

    @Override
    public void prepare() {
        // Prepare Beef Burger
        System.out.println("Preparing Beef Burger...");
    }

    @Override
    public void orderBurger() {
            System.out.println("Ordering Burger...");
            prepare();
    }

}