//Concrete Product
public class VeggieBurger implements Burger {

    @Override
    public void prepare() {
        // Prepare Veggie Burger
        System.out.println("Preparing Veggie Burger...");
    }

    @Override
    public void orderBurger() {
            System.out.println("Ordering Burger...");
            prepare();

    }

}