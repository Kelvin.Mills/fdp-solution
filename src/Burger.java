//Product
public interface Burger {

    void prepare();
    void orderBurger();

}