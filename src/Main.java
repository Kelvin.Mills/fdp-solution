
public class Main {
    public static void main(String[] args) {

        Restaurant beefBurger = new BeefBurgerRestaurant();
        Application beefBurgerApplication = new Application(beefBurger);
        beefBurgerApplication.getBurger().orderBurger();

        Restaurant veggieBurger = new VeggieBurgerRestaurant();
        Application veggieBurgerApplication = new Application(veggieBurger);
        veggieBurgerApplication.getBurger().orderBurger();


    }
}