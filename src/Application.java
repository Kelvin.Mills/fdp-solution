public class Application {
    private Burger burger;

    public Application(Restaurant restaurant){
        burger = restaurant.createBurger();
    }

    public Burger getBurger() {
        return burger;
    }
}
